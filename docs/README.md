### The HTML documentation located in "templates/" directory is generated as follows:

1) Modify your markdown files: USAGE_*.md
2) Run md2html.sh script:

    bash md2html.sh "markdown file name" "html file name" "html page title"


### Examples:

    bash md2html.sh USAGE_FR.md doc.html "RESIF: RESIFWS: Evalresp Docs: v1"
    bash md2html.sh USAGE_EN.md doc_en.html "RESIF: RESIFWS: Evalresp Docs: v1"


### Requirements:

pandoc
