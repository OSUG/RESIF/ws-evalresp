from apps.globals import HEIGHT
from apps.globals import PLOT_COLOR
from apps.globals import WIDTH


class Parameters:
    def __init__(self):
        self.network = None
        self.station = None
        self.location = None
        self.channel = None
        self.net = "*"
        self.sta = "*"
        self.loc = "*"
        self.cha = "*"
        self.time = None
        self.height = HEIGHT
        self.width = WIDTH
        self.color = PLOT_COLOR
        self.degrees = "false"
        self.annotate = "true"
        self.minfreq = 0.00001
        self.maxfreq = None
        self.nfreq = 200
        self.spacing = "log"
        self.units = "DEF"
        self.format = None
        self.nodata = "204"
        self.constraints = {
            "alias": [
                ("network", "net"),
                ("station", "sta"),
                ("location", "loc"),
                ("channel", "cha"),
            ],
            "booleans": ["annotate", "degrees"],
            "floats": ["minfreq", "maxfreq"],
            "not_none": ["format"],
        }

    def todict(self):
        return self.__dict__
